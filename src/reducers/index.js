import {combineReducers} from "redux";
import alert from "./alert";
import auth from "./auth";
import result from "./result";

export default combineReducers({
  alert,
  auth,
  result
});
