import {
    SAVE_RANDOM_RESULT,
    SAVE_USER_RESULT
} from "../actions/types"

const initialState = [{}]

export default function _(state = initialState, action) {
    const { type, payload } = action;
    switch (type) {
        case SAVE_RANDOM_RESULT:
            return [...state, payload];
        case SAVE_USER_RESULT:
            return [...state, payload];
        default:
            return state;
    }
}
