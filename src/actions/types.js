export const SET_ALERT = "SET_ALERT";
export const REMOVE_ALERT = "REMOVE_ALERT";

export const USER_LOADED = "USER_LOADED";
export const AUTH_ERROR = "AUTH_ERROR";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAIL = "LOGIN_FAIL";
export const LOGOUT = "LOGOUT";
export const REGISTER_FAIL = "LOGIN_FAIL";
export const REGISTER_SUCCES = "REGISTER_SUCCES";

export const SAVE_USER_RESULT = "SAVE_USER_RESULT";
export const SAVE_RANDOM_RESULT = "SAVE_RANDOM_RESULT";
export const SAVE_FAILED = "SAVE_FAILED";
