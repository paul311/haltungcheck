import React, {useState} from "react";
import {Link, Redirect} from "react-router-dom";
import {connect} from "react-redux";
import {setAlert} from "../actions/alert";
import PropTypes from "prop-types";
import {register} from "../actions/auth";

const Register = ({setAlert, register, isAuthenticated}) => {

  const [formData, setFormData] = useState({name: "", email: "", password: "", password2: ""});

  const {name, email, password, password2} = formData;

  const onChange = e => setFormData({
    ...formData,
    [e.target.name]: e.target.value
  })

  const onSubmit = async e => {
    e.preventDefault();
    if (password !== password2) {
      //Alert message and type
      setAlert("Passwörter stimmen nicht über ein!", "danger")

    } else {
      register({name, email, password})
    }
  }
  if (isAuthenticated) {
    return <Redirect to="/"/>
  }

  return <div>
    <div className="avoidNav margin-bottom"></div>
    <div className="d-flex justify-content-center">
      <section className="container">
        <h1 className="text-center">Jetzt Registrieren</h1>
        <p className="margin-bottom-big text-center">
          Erstelle deinen Account</p>
        <form className="form text-center" onSubmit={e => onSubmit(e)}>
          <div className="form-group">
            <input type="text" placeholder="Name" name="name" required="required" value={name} onChange={e => onChange(e)}/>
          </div>
          <div className="form-group">
            <input type="email" placeholder="Email Addresse" name="email" value={email} onChange={e => onChange(e)}/>

          </div>
          <div className="form-group">
            <input type="password" placeholder="Passwort" name="password" minLength="6" value={password} onChange={e => onChange(e)}/>
          </div>
          <div className="form-group">
            <input type="password" placeholder="Passwort bestätigen" name="password2" minLength="6" value={password2} onChange={e => onChange(e)}/>
          </div>
          <input type="submit" className="btn header-button margin-bottom-big white" value="Register"/>
        </form>
        <p className="my-1 text-center">
          Du hast schon einen Account?
          <Link to="/login"> Einloggen</Link>
        </p>
      </section>
    </div>
  </div>
}

Register.propTypes = {
  register: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool,
  setAlert: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
})
export default connect(mapStateToProps, {setAlert, register})(Register)
