import {Link} from "react-router-dom";
import {Fragment} from "react";
import HomeVideo from "./components/videos/homeVideo"

function Home() {
  // const player = <Player autoPlay poster="/images/home.jpg" src="/videos/kartenspielV4.mp4">
  //   <ControlBar disableCompletely={true}/>
  // </Player>
  //
  // console.log(player)

  // player.play(player)

  return <Fragment>
    <div className="avoidNav"></div>
    <HomeVideo/>

    <div className="d-flex home__button justify-content-center">
      <button className="btn w-50">
        <Link to="/game" preventscroll="true">
          <img alt="button" className="w-100" src="/images/button.png"></img>
        </Link>
      </button>
    </div>
    <div className="container">
      <div className="row home__teaser">
        <h2 className=" col-lg-7 col-xl-8 offset-1 col-10">
          SPIELEN SIE HIER UND JETZT DEN HALTUNGSCHECK.
        </h2>
        <p className=" col-lg-8 col-xl-8 offset-1 col-10 lead">
          Erfahren Sie, welche Haltungen Sie leben. In verschiedenen Umwelten. Reflektierend Ihre innere Haltung erweitern.
        </p>

      </div>
      <div className="row home__buttons d-flex justify-content-center">
        <div className="">
          <button className="home-nav__button border-thick">
            <Link className="no-decoration" to="/game">
              <img className="w-100 home-nav__button__img" alt="button" src="/images/home-button-1.png"></img>
              <div className="position-relative text-left home__button__text-1">
                <p className="lead">Zum</p>
                <h2>Haltungs-check.</h2>
              </div>

            </Link>
          </button>
        </div>

        <div className="">
          <button className="home-nav__button border-thick">
            <Link className="no-decoration" to="/erfahre-mehr">
              <img className="h-100 home-nav__button__img" alt="button" src="/images/home-button-2.png"></img>
              <div className="position-relative text-left home__button__text-2">
                <p className="lead">Das Modell der sechs</p>
                <h2>Haltungen</h2>
              </div>

            </Link>
          </button>
        </div>
        <div className="">
          <button className="home-nav__button border-thick">
            <a target="_blank" rel="noopener noreferrer" className="no-decoration" href="https://shop.short-cuts.de/collections/kartenspiele/products/kartenspiel-zur-erweiterung-der-inneren-haltung">
              <img className="w-100 home-nav__button__img" alt="button" src="/images/home-button-3.png"></img>
              <div className="position-relative text-left home__button__text-3">
                <p className="lead">Interesse?</p>
                <h2>Jetzt bestellen.</h2>
              </div>

            </a>
          </button>
        </div>
      </div>
      <div className="row home__audience">
        <h2 className="col-lg-7 col-xl-5 offset-1 col-10">
          Für Unternehmer und Selbstentwickler.
        </h2>
        <p className=" col-lg-8 offset-1 col-10">
          Wollen wir Führung und Unternehmenskultur zukunftsfähig gestalten, ist unsere Haltung entscheidend. Die Digitalisierung, der Gender Shift und die demografische Entwicklung haben einen Wandel eingeleitet, der Führungskräfte vor ganz neue Herausforderungen stellt. Die bisherigen Denkmuster greifen nicht mehr und es besteht Unsicherheit über die Richtung, in die wir uns entwickeln könnten. Für neue Lösungen braucht es alternative Denkweisen. Der mögliche gesellschaftliche und kulturelle Wandel beginnt mit einer erweiterten inneren Haltung.
        </p>
      </div>
    </div>

  </Fragment>

}

export default Home;
