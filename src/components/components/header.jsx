import { Link } from "react-router-dom";
import { logout } from "../../actions/auth";
import {connect} from "react-redux";
import PropTypes from "prop-types"

function Heading({ auth: { isAuthenticated, loading }, logout, title }) {
  let auth = <Link className="nav-link" to="/login"><div className="">
    <h4>Login</h4>
  </div>
  </Link>

  let game = <Link className="nav-link" to="/game"><div className="header-button header-button-text">HALTUNGSCHECK</div>
    </Link>

  if (isAuthenticated) {
    auth = <button className="btn" onClick={logout}>
        Logout
      </button>

    game = <Link className="nav-link" to="/game/me"><div className="header-button header-button-text">HALTUNGSCHECK</div>
    </Link>
  }

  

  return <div>
    <nav className="w-100 row navbar navbar-expand-lg navbar-light bg-light">

      <Link className="navbar-brand col-1 offset-lg-1 offset-md-0" to="/"><img alt="Icon" className="icon" src="/images/Icon.png" /></Link>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse offset-1" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">

          <li className="nav-item">
            <Link className="nav-link" to="/erfahre-mehr"><h4>DIE 6 HALTUNGEN</h4>
          </Link>
          </li>

          <li className="nav-item">
            <a className="nav-link" target="_blank" rel="noopener noreferrer" href="https://shop.short-cuts.de/collections/kartenspiele/products/kartenspiel-zur-erweiterung-der-inneren-haltung"><h4>KARTENSET BESTELLEN</h4>
            </a>
          </li>

          <li className="nav-item">
            {game}
          </li>

        </ul>
        <li className="nav-item">
          {auth}
        </li>
      </div>

    </nav>

  </div>
}
Heading.propTypes = {
  logout: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,

}
const mapStateToProps = state => ({
    auth: state.auth,
})
export default connect(mapStateToProps, {logout})(Heading);
