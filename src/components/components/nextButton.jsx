import { Link } from "react-router-dom";
import { results } from "../game.jsx"

function NextButton(props) {
  if (results.length === props.step) {
    return <button className="btn greyColor btn-hover-grey next__button--placeholder">Nächste</button>
  } else if (props.step === 15) {
    return <button onClick={props.increase} className="btn">
      <Link className="white nex__button btn-hover-white" to="/result">Result</Link>
    </button>
  } else {
    return <button className="btn btn-hover-white-inverse white next__button border-thick" onClick={props.increase}>Nächste</button>
  }
}

export default NextButton;
