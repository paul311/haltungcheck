function BackButton(props) {
  if (props.step === 0) {
    return <button className="btn btn-hover-grey greyColor">VORHERIGE</button>
  } else {
    return <button className="btn btn-hover-white white" onClick={props.decrease}>VORHERIGE</button>
  }
}

export default BackButton;
