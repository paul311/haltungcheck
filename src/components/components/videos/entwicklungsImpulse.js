function EntwicklungsImpulseVideo() {
  return <iframe width="560" height="315" title="Entwicklungsimpulse" src="https://www.youtube.com/embed/bBd2gAQHYFo?start=3" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
}
function TeamEntwicklungVideo() {
  return <iframe title="TeamEntwicklung" width="560" height="315" src="https://www.youtube.com/embed/R8z8WF7wlF4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
}
function FührungsVideo() {
  return <iframe title="FührungsVideo" width="560" height="315" src="https://www.youtube.com/embed/DuuanZSitfI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
}
function PersönlicheEntwicklung() {
  return <iframe title="PersönlicheEntwicklung" width="560" height="315" src="https://www.youtube.com/embed/KTvV39FOMKY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
}
export { EntwicklungsImpulseVideo, TeamEntwicklungVideo, FührungsVideo, PersönlicheEntwicklung }
