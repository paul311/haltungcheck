import React, { Component } from 'react';
import {Player, ControlBar} from 'video-react';


class HomeVideo extends Component{
    constructor(props) {
      super(props);
        this._child = React.createRef();
        this.poster = ""
    }

    componentDidMount() {

        this._child.current.play();

    }



    render() {
        return (
            <div>
              <Player ref={this._child} poster={this.poster} src="/videos/kartenspielV4.mp4">

                <ControlBar disableCompletely={true}/>
              </Player>
            </div>
        );
    }
}


export default HomeVideo
