

function Headline(props) {
  if (props.step < 4) {
    return <div className="d-flex offset-md-2 offset-1 col-10">
      <img className="margin-right h-75" alt="Target" src="/images/quadrant-light.png"/>
      <h2 className="white game__headline">Wie erleben sie Selbstentwicklung in ihrem Umfeld?</h2>
    </div>
  } else if (props.step >= 4 && props.step < 8) {
    return <div className="d-flex offset-md-2 offset-1 col-10">
      <img className="margin-right h-75 quadrant-2" alt="Target" src="/images/quadrant-light.png"/>
      <h2 className="white  game__headline">Wie wird in Ihrem Team miteinander Umgegangen?</h2>
    </div>
  } else if (props.step >= 8 && props.step < 12) {
    return <div className="d-flex offset-md-2 offset-1 col-10">
      <img className="margin-right quadrant-3 h-75" alt="Target" src="/images/quadrant-light.png"/>
      <h2 className="white  game__headline">Wie gestalten sich Strukturen und Prozessen in Ihrer Umgebung?</h2>
    </div>
  } else if (props.step >= 12 && props.step < 16) {
    return <div className="d-flex offset-md-2 offset-1 col-10">
      <img className="margin-right quadrant-4 h-75" alt="Target" src="/images/quadrant-light.png"/>
      <h2 className="white  game__headline">Welche Kultur wird in Ihrem Umfeld gelebt.</h2>
    </div>
  } else {
    return null;
  }
}

export default Headline
