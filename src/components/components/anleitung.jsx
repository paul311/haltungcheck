

function Anleitung() {
  return <section id="Anleitung">
    <div className="anleitung d-flex justify-content-center">
      <div className="row container">
        <div className="anleitung__margin">
          <h2 className="col-10 offset-1">
            Die Entwicklung des Targets
          </h2>
          <p className="col-10 offset-1">
            Das Target ist die Grundlage, auf der du alle Haltungskarten verorten kannst. Egal, für welche Aussagen du dich entschieden hast: Hier kannst du sehen, in welcher Haltung du dich am wohlsten fühlst. An den Farben der Ringe erkennst du, in welcher Haltung du unterwegs bist und was dein nächster Entwicklungsschritt sein könnte. Die Quadranten zeigen dir, ob du Aussagen im Bereich Selbstentwicklung, Teamentwicklung, Strukturen und Prozesse oder Kulturentwicklung getroffen hast. Du wirst feststellen, dass du deine eigene Tendenz hast und sich damit spannende Entwicklungsschwerpunkte ergeben. Das Wichtigste: Alle Haltungen machen Sinn und bauen aufeinander auf. Es gibt kein richtig oder falsch.
          </p>
        </div>

        <div>
          <img alt="Target" className="col-10 offset-1 anleitung__margin" src="/images/target.jpg"/>
        </div>

        <h2 className="col-10 offset-1">
          Die Quadranten
        </h2>
        <p className="col-10 offset-1 anleitung__margin">
          Bei der „Selbstentwicklung“ geht es darum, welche Haltungen deine Persönlichkeit ausmachen und deine individuelle Vorstellung von der Welt prägen. Im Bereich „Teamentwicklung“ werden deine Bedürfnisse sichtbar, die dir im Arbeitskontext wichtig sind: Brauchst du Freiraum oder eher klare Vorgaben? Präferierst du Team- oder Einzelarbeit? „Strukturen und Prozesse“ verdeutlicht, welche Denkstrukturen in eurer Organisation vorherrschen, wie Prozesse organisiert sind oder die Kommunikation läuft. Der Quadrant „Kultur“ macht sichtbar, welches Bewusstsein ihr über eure Verantwortung und auch eure Möglichkeiten habt, globale Themen zu beeinflussen.
        </p>
        <div className="offset-md-1 col-md-5 d-flex margin-bottom margin-right">
          <div className="margin-right align-self-center">
            <img alt="Target" className="" src="/images/quadrant-1.png"/>
          </div>
          <div className="">
            <h5 className="">
              Selbstentwicklung
            </h5>
            <p>
              Führungshaltung, Haltung, Biografische Entwicklung, Paradigma
            </p>
          </div>
        </div>
        <div className=" col-md-5 d-flex margin-bottom margin-right">
          <div className="margin-right align-self-center">
            <img alt="Target" className="" src="/images/quadrant-3.png"/>
          </div>
          <div className="">
            <h5 className="">
              Strukturen und Prozesse:
            </h5>
            <p>
              Bewusstsein, Denkstruktur, Innovationskultur, Organigramm
            </p>
          </div>
        </div>
        <div className="offset-md-1 col-md-5 d-flex margin-bottom margin-right">
          <div className="margin-right align-self-center">
            <img alt="Target" className="" src="/images/quadrant-2.png"/>
          </div>
          <div className="">
            <h5 className="">
              Teamentwicklung:

            </h5>
            <p>
              Situative Haltung, Beziehung, Handlungsimpuls, Teamspirit
            </p>
          </div>
        </div>
        <div className=" col-md-5 d-flex margin-bottom margin-right">
          <div className="margin-right align-self-center">
            <img alt="Target" className="" src="/images/quadrant-4.png"/>
          </div>
          <div className="">
            <h5 className="">
              Kulturentwicklung:

            </h5>
            <p>
              Organisationsform, Arbeitswelt
            </p>
          </div>
        </div>


      </div>

    </div>
  </section>

}

export default Anleitung;
