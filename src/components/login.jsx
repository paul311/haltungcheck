import React, {Fragment, useState} from "react";
import {Link, Redirect} from "react-router-dom";
import { login } from "../actions/auth";
import {connect} from "react-redux";
import PropTypes from "prop-types"

const Login = ({login, isAuthenticated}) => {
  const [formData, setFormData] = useState({email: "", password: ""});

  const {email, password} = formData;

  //For every Form thingy thorugh e.target.value
  const onChange = e => setFormData({
    ...formData,
    [e.target.name]: e.target.value
  });

  const onSubmit = async e => {
    e.preventDefault();
    login(email, password)

  }

  // REDIRECT if loged in
  if (isAuthenticated) {
      return <Redirect to="/" />
  }
  return <Fragment>
    <div className="avoidNav margin-bottom">

    </div>
    <div className="d-flex justify-content-center">
      <section className="container">
        <h2 className=" text-center">Log dich ein</h2>
        <p className="text-center margin-bottom-big">
          Log dich ein und schalte Spielmodi frei</p>
        <form className="form text-center" onSubmit={e => onSubmit(e)}>
          <div className="form-group">
            <input type="email" placeholder="Email Addresse" name="email" value={email} onChange={e => onChange(e)}/>
          </div>
          <div className="form-group">
            <input type="password" placeholder="Passwort" name="password" minLength="6" value={password} onChange={e => onChange(e)}/>
          </div>

          <input type="submit" className="btn header-button margin-bottom-big white" value="Login"/>
        </form>
        <p className="my-1 text-center">
          Du hast noch keinen Account?
          <Link to="/register"> Registrieren</Link>
        </p>
      </section>
    </div>

  </Fragment>
}
Login.propTypes = {
    login: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool,
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
})

export default connect(mapStateToProps, { login })(Login)
