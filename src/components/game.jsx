import React, { useState, Fragment } from "react";
import data from "../data.js";
import { Redirect} from "react-router-dom";

import Gamecontrols from "./components/gamecontrols.jsx";
import Headline from "./components/headline.jsx";
import Anleitung from "./components/anleitung.jsx";

import { saveResult } from "../actions/result";
import {connect} from "react-redux";
import PropTypes from "prop-types"

//stores selected ids
let results = [];
const randomPositions = [[2, 4, 6, 3, 5, 1], [2, 5, 3, 6, 1, 4], [5, 4, 6, 1, 2, 3], [5, 2, 6, 4, 3, 1], [2, 5, 6, 1, 3, 4], [5, 1, 3, 2, 6, 4], [5, 6, 4, 1, 3, 2], [6, 2, 4, 5, 3, 1], [3, 2, 6, 5, 1, 4], [6, 5, 2, 1, 4, 3], [4, 5, 2, 3, 6, 1], [2, 4, 1, 3, 5, 6], [2, 3, 1, 6, 5, 4], [3, 1, 5, 2, 6, 4], [1, 4, 3, 6, 2, 5], [3, 5, 6, 1, 2, 4], [1, 3, 2, 6, 5, 4], [3, 4, 1, 6, 2, 5], [5, 3, 1, 2, 4, 6], [4, 1, 3, 6, 2, 5], [6, 1, 4, 3, 5, 2], [3, 6, 4, 1, 5, 2], [4, 3, 6, 5, 2, 1], [4, 6, 5, 3, 1, 2], [2, 3, 1, 6, 5, 4], [1, 6, 2, 4, 3, 5], [4, 1, 3, 2, 6, 5], [1, 6, 4, 5, 3, 2], [3, 2, 4, 6, 1, 5], [4, 6, 5, 1, 3, 2], [1, 2, 6, 4, 3, 5], [3, 4, 1, 6, 5, 2], [2, 6, 1, 3, 4, 5], [5, 6, 4, 3, 2, 1], [2, 4, 5, 3, 6, 1], [5, 2, 4, 6, 1, 3], [2, 5, 3, 1, 6, 4], [3, 6, 2, 5, 1, 4], [6, 1, 5, 4, 2, 3], [2, 1, 5, 4, 3, 6], [2, 5, 1, 4, 3, 6], [5, 3, 4, 6, 2, 1], [4, 1, 2, 5, 6, 3], [4, 6, 1, 5, 3, 2], [5, 1, 2, 4, 6, 3], [2, 1, 5, 4, 6, 3], [4, 5, 3, 6, 1, 2], [5, 4, 6, 2, 1, 3], [2, 5, 3, 6, 1, 4], [5, 6, 1, 4, 3, 2]];

const shuffeledData = [];
function shuffleData() {
  for (let i = 0; i < 16; i++) {
    const randomElement = randomPositions[Math.floor(Math.random() * randomPositions.length)];
    randomElement.forEach(index => shuffeledData.push(data[i * 6 + index - 1]))
  }

}

shuffleData();

function Game({isAuthenticated, saveResult}) {

  //for which step
  const [step, setStep] = useState(0);
  //just used to display nextButton
  const [count, setCount] = useState(0);


  // increases step value
  async function increase() {
    await setStep(step + 1);
  }

  function restart() {
    results = [];
    setStep(0);
    setCount(0);
    shuffleData();
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    });
    }

  if (step === 16 ) {
    const data = results;
    console.log(1)
    saveResult({data, isAuthenticated})
    return <Redirect to="/result" />
  }
  if (step === 0 && results.length > 0) {
    restart()
  }

  // decreases step value and deletes previous results
  async function decrease() {
    if (results.length === (step + 1)) {
      for (let i = 0; i < 2; i++) {
        results.pop();
      }
    } else {
      results.pop();
    }
    console.log(results)
    await setStep(step - 1);
  }

  return <Fragment>

    <div className="game" id="game">

      <div className="avoidNav margin-bottom">

      </div>
      <div className="d-flex justify-content-center row">
        <div className="cotainer">
          <Headline step={step} />
        </div>

      </div>
<div className="d-flex justify-content-center">


      <div className="d-flex row game__gameplay justify-content-center container ">

          <Gamecontrols increase={increase} decrease={decrease} count={count} step={step}/>


        <div className="d-flex col-xl-8 col-lg-8 col-md-11 col-11  flex-wrap margin-bottom justify-content-center">
          {
          shuffeledData.map((pic, index) => {
              if ((index < (step * 6 + 6)) && (index >= (step * 6))) {

                if (pic.id === results[results.length -1]) {
                    var selected = "selected"
                } else {
                     selected = "notSelected"
                }
                return <Pic key={pic.id} increase={increase} id={pic.id} imgPath={pic.imgPathFront} selected={selected} step={step} count={count} setCount={setCount} />


              } else {
                return null
              }
            })
          }
        </div>
      </div>
      </div>





<div className="avoidNav"></div>
    </div>

    <Anleitung />
    <div className="d-flex justify-content-center w-100">
      <button onClick={restart} className="anleitung__link lead border-thick btn">
        Neu Starten
      </button>
    </div>
  </Fragment >
}

//##########
function Pic(props) {
  // const [selectedClass, setSelectedClass] = useState("notSelected");

  function toggle() {
    if ((props.step + 1) > results.length) {
      results.push(props.id)
      props.setCount(props.count + 1)
      console.log(results)
      props.increase()


        //change pictures here with setPictures
    } else {
      if (props.id === results[results.length -1]) {
        results.pop();
        props.setCount(props.count - 1)
      } else {
        results.pop();
        props.setCount(props.count - 1)
        results.push(props.id)
        console.log("hi")
        props.setCount(props.count + 1)
      }

      console.log(results)
    }
  }

  return <div className="game__bild d-flex justify-content-center col-lg-4 col-6">
    <button className={props.selected} onClick={toggle}><img className="werteBild" alt="werteBild" src={props.imgPath} /></button>
  </div>
}


export {
  results
}

Game.propTypes = {
    saveResult: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool,
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
})

export default connect(mapStateToProps, {saveResult})(Game);
