// import Goo from "./components/goo.jsx"
import ImageSlider from "./components/imageSlider.jsx";
import Wissensdrang from "./components/wissensdrang.jsx";
import {EntwicklungsImpulseVideo} from "./components/videos/entwicklungsImpulse.js";
import {Fragment} from "react";


function ErfahreMehr() {
  return <Fragment>
    <div className="avoidNav"></div>
    <div className="d-flex justify-content-center">
    <div className="row container">
      <div className="offset-lg-2 col-lg-8 col-12 margin-bottom-huge">
        <ImageSlider/>
      </div>
      <a className="col-12 text-center" href="#Explanation">
        <h2 className="margin-bottom">Die 6 Haltungen</h2>
        <img className="result__go-down" src="/images/result-go-down.png" alt="go-down"/>
        <hr id="Explanation" className="margin-bottom-big"/>
      </a>

      <div className="offset-lg-2 col-lg-8 col-12">
        <h2 className="text-green">
          Reise durch die Haltungen
        </h2>
        <p className="margin-bottom-big">
          Eines ist wichtig zu wissen: Es gibt weder gute noch schlechte Haltungen. Wir alle greifen in den Situationen unseres Alltages auf unterschiedliche Haltungen zurück. Manchmal sind sie hilfreich und manchmal stehen sie uns im Weg. Jeder von uns hat aber Haltungsschwerpunkte. Das Target hilft dir dabei, sie sichtbar zu machen. Sieh einfach, in welchen Haltungen, Quadranten und Achsen Du die meisten Karten gewählt hast und mache dir ein Bild davon. Nichts ist dabei statisch festgelegt. Haltungen sind nicht normativ – Betrachte alle Aussagen als Anregung für deinen Entwicklungsprozess und Reflexionsmöglichkeit.
        </p>
        <div className="d-flex justify-content-center margin-bottom-huge">
          <EntwicklungsImpulseVideo/>
        </div>
      </div>
      <div>
        <div className="margin-bottom-huge">
          <div className="selbstorientiert d-flex align-items-center flex-lg-row flex-column margin-bottom-gigantic">
            <div className="offset-lg-1 col-lg-6 col-12 offset-0 order-lg-0 order-1">
              <h2 className="white">Selbstorientiert-impulsive Haltung</h2>
              <p className="white">
                In der selbstorientiert-impulsiven Haltung sind wir stark in uns und unseren Bedürfnissen gefangen. Reflektierendes Denken und das Erfassen längerer Zeiträume ist in dieser Haltung noch nicht möglich. Feedback wird zurückgewiesen und wir bleiben im stereotypen Denken, das sich vor allem auf Konkretes und wenig auf Abstraktes bezieht. Unser eigenes emotionales Erleben können wir noch nicht erfassen oder steuern. Es fehlt die Kompetenz, anderen Menschen empathisch zu begegnen. Die Denkweisen sind eher simpel, die anderen sind immer schuld. Wir sind tendenziell in einer Verteidigungshaltung, weil es an innerer Sicherheit und echtem Selbstbewusstheit fehlt.
              </p>
            </div>
            <img className="order-0 oder-lg-5 haltungs-slider__icon" src="/images/selbstorientiert-1.png" alt="go-down"/>
            <img className="order-8" src="/images/selbstorientiert-2.png" alt="go-down"/>
          </div>
          <div className="gemeinschaftsbestimmt d-flex align-items-center flex-lg-row flex-column margin-bottom-gigantic">

            <div className=" col-lg-6 col-12 offset-0 offset-lg-1 order-lg-2 order-1">
              <h2 className="white">Gemeinschaftsbestimmt- Konformistische Haltung</h2>
              <p className="white">
                In dieser Haltung lernen wir Regeln und Normen, die sich an unserem Umfeld ausrichten. Unsere Identität wird stark durch die Zugehörigkeit zu einem Wir definiert und weniger durch unsere Individualität. Gehorsam und Unterordnung sind in dieser Haltung vorherrschend. Damit verbunden sind starke Schuldgefühle, wenn wir den Konventionen nicht entsprechen. Wir stehen unter einem hohen Anpassungsdruck, der uns Konflikte vermeiden lässt. Unsere eigenen Gefühle und unser Innenleben sind für uns noch schlecht greifbar. Kritik wird akzeptiert, wenn sie sich auf Prinzipien bezieht, die extern festgelegt wurden.
              </p>
            </div>
            <img className="order-8 order-lg-0 margin-right" src="/images/gemeinschaftsbestimmt-2.png" alt="go-down"/>
            <img className="order-0 oder-lg-1 haltungs-slider__icon" src="/images/gemeinschaftsbestimmt-1.png" alt="go-down"/>
          </div>
          <div className="rationalistisch selbstorientiert d-flex align-items-center flex-lg-row flex-column margin-bottom-gigantic">
            <div className="offset-lg-1 col-lg-6 col-12 offset-0  order-lg-0 order-1">
              <h2 className="white">Rationalistisch- Funktionale Haltung
              </h2>
              <p className="white">
                Die rationalistisch-funktionale Haltung ist die nächste Kompetenzerweiterung und der Beginn des psychologischen Ich. Die beginnende Selbstwahrnehmung erlaubt uns einen differenzierten Blick auf uns selbst. Jetzt können wir verschiedene Perspektiven sehen und werden urteilsfreier. Der Wunsch nach mehr eigener Meinung und nach Abgrenzung entsteht. Wir entwickeln eigene Ansichten darüber, was richtig und falsch ist. Von langen Debatten halten wir wenig. Uns selbst erleben wir noch von externen Anforderungen getrieben, innerhalb derer wir glauben, funktionieren zu müssen. In unserer eigenen Entwicklung durchlaufen wir diese Haltung in der Regel mit Beginn der Pubertät.
              </p>
            </div>
            <img className="order-0 oder-lg-5 haltungs-slider__icon" src="/images/rationalistisch-1.png" alt="go-down"/>
            <img className="order-8" src="/images/rationalistisch-2.png" alt="go-down"/>
          </div>
          <div className="gemeinschaftsbestimmt eigenbestimmt d-flex align-items-center flex-lg-row flex-column margin-bottom-gigantic">

            <div className=" col-lg-6 col-12 offset-lg-1 offset-0 order-lg-2 order-1">
              <h2 className="white">Eigenbestimmt- Souveräne Haltung
              </h2>
              <p className="white">
                Mit der eigenbestimmt-souveränen Haltung erwerben wir eigene Werte und Vorstellungen. Wir entwickeln eine starke Zielorientierung und uns treibt der Wunsch nach Selbstoptimierung. Es entfaltet sich ein vielfältigeres Innenleben, das die Komplexität von Situationen akzeptiert und Respekt vor individuellen Unterschieden hat. Die eigenen blinden Flecken und die eigene Subjektivität werden häufig noch nicht gesehen. Das „Ego“ ist in dieser Haltung am größten. Es entspricht dem eines späten Teenagers, der vieles weiß und kompetent ist, aber dessen Empa­thievermögen sich noch nicht ganz entfaltet hat.
              </p>
            </div>
            <img className="order-8 order-lg-0 margin-right" src="/images/eigenbestimmt-2.png" alt="go-down"/>
            <img className="order-0 oder-lg-1 haltungs-slider__icon" src="/images/eigenbestimmt-1.png" alt="go-down"/>
          </div>
          <div className="selbstorientiert relativistisch d-flex align-items-center flex-lg-row flex-column margin-bottom-gigantic">
            <div className="offset-lg-1 col-lg-6 col-12 offset-0 order-lg-0 order-1">
              <h2 className="white">Relativierend-
Individualistische Haltung</h2>
              <p className="white">
                Die relativierend-individualistische Haltung macht uns bewusst, wie die eigene Wahrnehmung die Sicht auf die Welt prägt. Wir fangen an, unsere und die Sichtweise anderer zu relativieren und zu hinterfragen. Mit dieser Kompetenz entwickelt sich unser Empathievermögen weiter. Wir sehen, dass jeder Mensch geprägt ist durch seine grundlegenden Eigenschaften, seine Kultur und seine eigene Geschichte. Wir lernen, dies in unserer Kommunikation zu berücksichtigen. In dieser Haltung wird uns unser emotionales Innenleben bewusster und wir entdecken es als zusätzliche relevante Wahrnehmungsressource.
              </p>
            </div>
            <img className="order-0 oder-lg-5 haltungs-slider__icon" src="/images/relativistisch-1.png" alt="go-down"/>
            <img className="order-8" src="/images/relativistisch-2.png" alt="go-down"/>
          </div>
          <div className="systemisch gemeinschaftsbestimmt d-flex align-items-center flex-lg-row flex-column margin-bottom-gigantic">

            <div className=" col-lg-6 col-12 offset-0 offset-xl-1 order-lg-2 order-1">
              <h2 className="white">Systemisch-
Autonome Haltung</h2>
              <p className="white">
              Mit der systemisch-autonomen Haltung erweitern sich unsere Kompetenzen um die Fähigkeit zur voll ausgebildeten Multiperspektivität. In dieser Haltung sind wir offen für die kreative Auseinandersetzung mit Konflikten und können mit Mehrdeutigkeit umgehen. Wir respektieren die Individualität und Autonomie unseres Gegenübers und sind bereit, die volle Verantwortung für uns selbst, unser Denken, Fühlen und Handeln zu übernehmen. Wir können unsere Gedanken und Gefühle als subjektiv erkennen und haben mehr kooperative Handlungsoptionen. Es entsteht ein Konstruktgewahrsein über die eigenen subjektiven Deutungsmuster.
              </p>
            </div>
            <img className="order-8 order-lg-0 margin-right" src="/images/systemisch-2.png" alt="go-down"/>
            <img className="order-0 oder-lg-1 haltungs-slider__icon" src="/images/systemisch-1.png" alt="go-down"/>
          </div>

        </div>


      </div>
      <Wissensdrang/>
      </div>
    </div>

    {/* <Goo /> */}

  </Fragment>
}

export default ErfahreMehr;
