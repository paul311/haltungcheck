import {results} from "./game.jsx";
import {Polar} from "react-chartjs-2";
import React, {useState} from 'react';

import data from "../data.js";

import BackSides from "./components/backsides.jsx";
import LoadingAnimation from "./components/loadingAnimation.jsx";
import options from "./components/options.js";
import Wissensdrang from "./components/wissensdrang.jsx";
// import Goo from "./components/goo.jsx"

const sektoren = [
  "Haltung",
  "Biografische Entwicklung",
  "Paradigma",
  "Situative Haltung",
  "Beziehung",
  "Handlungsimpuls",
  "Teamspirit",
  "Bewusstsein",
  "Denkstruktur",
  "Innovationskultur",
  "Organigram",
  "Organisationsfrom",
  "Arbbeitswelt",
  "Kommunikationskultur",
  "Führungsstil",
  "Führungshaltung"
]

function Result() {

  //selecting FinalResults
  const finalResults = [];
  results.forEach(item => finalResults.push(data[item - 1]));
  const selbstEntwicklung = []
  const teamEntwicklung = []
  const strukturenUndProzesse = []
  const kulturEntwicklung = []

  // Assigning to Sectors
  const haltungen = []
  for (let i = 0; i < 4; i++) {
    selbstEntwicklung.push(finalResults[i])
  }
  for (let i = 4; i < 8; i++) {
    teamEntwicklung.push(finalResults[i])
  }
  for (let i = 8; i < 12; i++) {
    strukturenUndProzesse.push(finalResults[i])
  }
  for (let i = 12; i < 16; i++) {
    kulturEntwicklung.push(finalResults[i])
  }

  finalResults.forEach(item => haltungen.push(item.haltung + 1))

  const dataset = {
    labels: sektoren,
    datasets: [
      {
        label: "Dein Haltungsraum",
        borderJoinStyle: "round",
        borderColor: "white",
        borderWidth: "0px",
        backgroundColor: function(context) {
          var index = context.dataIndex;
          var value = context.dataset.data[index];
          switch (value) {
            case 2:
              return "#e65014";
            case 3:
              return "#b99655";
            case 4:
              return "#5087c3";
            case 5:
              return "#f5a500";
            case 6:
              return '#87be2d';
            case 7:
              return "#2db4aa";
            default:
              return "black"
          }

        },
        data: haltungen
      }
    ]
  }


  const [loading, setLoading] = useState(true);
  if (loading === true) {
    setTimeout(() => setLoading(false), 2000)
    return <LoadingAnimation/>
  } else {
    return <div>
      <div className="avoidNav margin-bottom"></div>
      <div className="">
        <div className="d-flex row justify-content-center items-align-center">
          <div className="col-11 col-sm-9 col-md-7 col-lg-6 col-xl-4 margin-bottom-big">
            <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>

              </ol>
              <div className="carousel-inner">
                <div className="carousel-item active">
                  <div className="d-block results__polar">
                    <Polar width={390} height={390} data={dataset} options={options}/>
                  </div>

                </div>
                <div className="carousel-item">
                  <img className="d-block w-100" src="/images/target.jpg" alt="Second slide"/></div>

              </div>
              <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span className="" aria-hidden="true"></span>
                <span className="sr-only">Previous</span>
              </a>
              <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span className="" aria-hidden="true"></span>
                <span className="sr-only">Next</span>
              </a>
            </div>

          </div>
          <a className="col-12 text-center" href="#BackSides">
            <h2 className="margin-bottom">Hier ist ihr Ergebnis.</h2>
            <img id="BackSides" className="result__go-down" src="/images/result-go-down.png" alt="go-down"/>
          </a>

        </div>
        <div className="d-flex justify-content-center">
          <BackSides results={results} selbstEntwicklung={selbstEntwicklung} teamEntwicklung={teamEntwicklung} kulturEntwicklung={kulturEntwicklung} strukturenUndProzesse={strukturenUndProzesse}/>
        </div>
        <div className="d-flex justify-content-center row">
          <div className="col-8 col-lg-3 col-md-4">
            <h2 className="text-center ">
              Haltung Erweitern
            </h2>
            <p className="text-center margin-bottom-big">
              Mit über 140 Karten und 5 verschiedenen Varianten!
            </p>
          </div>
          <div className="d-flex justify-content-center col-12 margin-bottom-big">
            <img className="w-50" src="/images/spiel.jpg" alt="Third slide"/>
          </div>
        </div>
        <div className="d-flex justify-content-center margin-bottom-big">
          <a href="https://shop.short-cuts.de/collections/kartenspiele/products/kartenspiel-zur-erweiterung-der-inneren-haltung" className="text-center no-decoration header-button white lead">
            Jetzt Bestellen
          </a>
        </div>
        <div className="d-flex justify-content-center margin-bottom-huge">
          <a href="/" className="text-center d-flex no-decoration lead">
            <h5>

              DIE 6 HALTUNGEN
            </h5>
            <img className="margin-left" src="/images/go-to-haltungen.png" alt="Third slide"/>
          </a>
        </div>
        <Wissensdrang />
      </div>
      {/* <hr/>
      <div className="result__goo">
        <Goo />
      </div> */
      }

    </div>

  }
}
export default Result // {
//   finalResults.map((pic, index) => {
//     if ((index+1) % 4 === 0) {
//       const br = <br/>
//       console.log(br)
//     }
//     return <Pic key={pic.id} id={pic.id} imgPath={pic.imgPathBack}/>
//
//   })
// }
