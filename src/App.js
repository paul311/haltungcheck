import {BrowserRouter as Router, Switch, Route} from "react-router-dom";

import './App.css';
import Game from './components/game.jsx';
import Result from './components/results.jsx';
import Home from './components/home.jsx';
import ErfahreMehr from './components/erfahreMehr.jsx';
import Heading from './components/components/header.jsx';
import Footer from './components/components/footer.jsx';
import Login from './components/login.jsx';
import Register from './components/register.jsx';
import Alert from './components/components/alert.jsx';

import PageNavigationListener from "./utils/PageNavigationListener.jsx";

//redux
import {Provider} from "react-redux";
import store from "./store";

function App() {
  return (<Provider store={store}>
    <Router>
      <PageNavigationListener/>
      <div className="App">

        {/* A <Switch> looks through its children <Route>s and
          renders the first one that matches the current URL. */
        }
        <Switch>
          <Route path="/game">
            <Heading title="Spiel LogIn"/>
            <Game/>
            <Footer/>
          </Route>
          <Route path="/game" >
            <Heading title="Spiel"/>
            <Game/>
            <Footer/>
          </Route>
          <Route path="/login" >
            <Heading title="Login"/>
            <Alert />
            <Login/>
            <Footer/>
          </Route>
          <Route path="/register" >
            <Heading title="Register"/>
            <Alert />
            <Register/>
            <Footer/>
          </Route>
          <Route path="/result">
            <Heading title="Dein Ergebnis"/>
            <Result/>
            <Footer/>
          </Route>
          <Route path="/erfahre-mehr">
            <Heading title="Erfahre Mehr"/>
            <ErfahreMehr/>
            <Footer/>
          </Route>
          <Route path="/">
            <Heading title="Haltung Erweitern"/>
            <Home/>
            <Footer/>
          </Route>
        </Switch>
      </div>
    </Router>
  </Provider>);
}

export default App;
